# pip install schedule
# textbelt api for sending message
# pip install request
# from credentials import mobile_number

import requests
import schedule
import time


def send_message():
    resp = requests.post('https://textbelt.com/text', {
  'phone': '+91-999999999',
  'message': 'Hello There!, its meeting time',
  'key': 'textbelt',
    })
    print(resp.json())

# schedule.every().day.at("9:30").do(send_message)

schedule.every(10).seconds.do(send_message)

while True:
    schedule.run_pending()
    time.sleep(1)
