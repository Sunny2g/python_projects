import random
exit = False
user_points = 0
computer_points = 0

while exit == False:
    
    options = ["rock", "paper", "scissors"]

    user_input = input("Choose rock, paper,scissors or exit: ")
    computer_input = random.choice(options)
    if user_input == "exit":
        print("Game ended")
        exit = True

    if user_input == "rock":
        if computer_input == "rock":
            print("your input is rock")
            print("computer input is paper")
            print("it is  a tie!")
            print(user_points)
            print(computer_points)
        elif computer_input == "paper":
            print("your input is rock")
            print("computer input is paper")
            print("computer wins")
            computer_points += 1
            print(user_points)
            print(computer_points)

        elif computer_input == "scissors":
            print("your input is rock")
            print("computer input is scissors")
            print("you wins")
            user_points += 1
            print(user_points)
            print(computer_points)

    elif user_input != "rock" or user_input != "paper" or user_input != scissors:
        print("user input is invalid!")
